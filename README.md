![Build Status](https://gitlab.com/pages/godot/badges/master/build.svg)

---

Example [Godot] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)
- [Pong Example Project](#pong-example-project)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

``` yaml
image: barichello/godot-ci:3.1.1

pages:
  script:
  - mkdir public
  - godot -v --export "HTML5" ./public/index.html
  artifacts:
    paths:
    - public
  only:
  - master
```

We're using a headless instance of godot to export the `HTML5` project template.
This is the default HTML5 template for Godot which will output an `index.wasm` binary
with the game's code along with an `index.pck` file for the assets, as well as
an `index.hmtl` and `index.js` to load it all.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] godot
1. Open the directory with Godot
1. Add content

Read more at Godot's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

## Pong Example Project

2D pong example project forked from
https://github.com/godotengine/godot-demo-projects/tree/master/2d/pong


[ci]: https://about.gitlab.com/gitlab-ci/
[godot]: https://godotengine.org/
[install]: https://godotengine.org/download
[documentation]: https://docs.godotengine.org/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

----

Forked from @ThibaultLemaire

