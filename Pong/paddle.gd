extends Area2D
class_name Pallette

export var ball_dir := 1.0

const MOVE_SPEED := 100.0

onready var boundary := get_viewport_rect().size.y / 2

func _process(delta):
	var which = get_name()
	
	# move up and down based on input
	if Input.is_action_pressed(which+"_move_up") and position.y > -boundary:
		position.y -= MOVE_SPEED * delta
	if Input.is_action_pressed(which+"_move_down") and position.y < boundary:
		position.y += MOVE_SPEED * delta
		

func _on_area_entered( area ):
	if area.get_name() == "ball":
		# assign new direction
		area.direction = Vector2(ball_dir, randf() * 2 - 1).normalized()
