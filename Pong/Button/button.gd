extends Node

func _on_pressed():
	# The button should have the name of the input it triggers
	Input.action_press(name)

func _on_released():
	Input.action_release(name)
